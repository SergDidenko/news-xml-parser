<?php $items = $variables['items']; ?>

<?php foreach ($items as $nid=>$news): ?>
  <div class="<?php print 'last-news'; ?>">
    <h4><a class="last-news_field-link" id="<?php print $nid; ?>" style="cursor: pointer;"><?php print $news->title; ?></a></h4>
  </div>
<?php endforeach; ?>