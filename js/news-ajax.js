(function($){
    Drupal.behaviors.ajaxnews = {
      attach: function(context, settings){
          $(context).find('.last-news_field-link').click(function(e){
              var id = $(this).attr('id');
              $.ajax({
                  url:'/ajax',
                  type:'POST',
                  data:{'id': id},
                  success: function(){
                      console.log('OK');
                  }
              });
              return false;
          });
      }
    };
})(jQuery);